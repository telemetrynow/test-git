var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/biciControllerApi');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
//router.post('/delete', bicicletaController.bicicleta_delete);
router.delete('/delete', bicicletaController.bicicleta_delete);
router.put('/update/:id', bicicletaController.bicicleta_update);


module.exports = router;