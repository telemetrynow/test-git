var mymap = L.map('main_map').setView([4.594911, -74.123508], 15);

// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/"> OpenStreetMap </a> contributors',
//     id: 'mapbox/streets-v11',
//     accessToken: 'pk.eyJ1Ijoibmljb2xhczk3IiwiYSI6ImNrOXV0MnM0ajA1N3MzZG8xeDVzcDY3c2MifQ.rUK2WQkFnDcBh5WsqxjNig',
// }).addTo(mymap);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
}).addTo(mymap);

//L.marker([4.594911, -74.123508]).addTo(mymap);
//L.marker([4.594343, -74.124808]).addTo(mymap);
//L.marker([4.594950, -74.122466]).addTo(mymap);
//L.marker([4.595190, -74.125778]).addTo(mymap);

$.ajax({
    dataType: 'json',
    headers: {
        'x-access-token': 'pk.eyJ1Ijoibmljb2xhczk3IiwiYSI6ImNrOXV0MnM0ajA1N3MzZG8xeDVzcDY3c2MifQ.rUK2WQkFnDcBh5WsqxjNig'
    },
    url: "api/bicicletas",
    success: function(result){
        //console.log(result)
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap)
        });
    }
})
 



