var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta')
var request = require('request')
var server = require('../../bin/www')

var base_url = 'http://localhost:3000/api/bicicletas'


describe('Bicicletas Api', () => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        //mongoose.connect(mongoDB, { useNewUrlParser: true});
        mongoose.connect(mongoDB, { useNewUrlParser: true});


        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log('This is the error: ', err);
            done();
        });
    });

    describe('Get Bicicletas /', () => {
        it('Debe devolver 200', (done) => {  
            //expect(Bicicleta.allBicis.length).toBe(0); 
            expect(0).toBe(0);
            //done();
            var aBici = new Bicicleta({ code:15, color:'violeta', modelo:'ciudad', ubicacion: [-34.6112424, -58.5412424] });
            Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err)
                    Bicicleta.allBicis(function(err, bicis){
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toBe(aBici.code);
                        
                        
                    });
                });

            request.get(base_url, function(error, response, body){

                    result = JSON.parse(body)
                    //console.log(result.bicicletas.length)
                    //console.log(aBici.code)
                    //console.log(result)
                    var codigo = result.bicicletas[0].code
                
                    expect(response.statusCode).toBe(200);
                    expect(result.bicicletas.length).toBe(1);
                    expect(codigo).toBe(aBici.code);
                    
                    done();
                });
        });
    });
    
    describe('Post Bicicletas /create', () => {
        it('STATUS 200', (done) => {    

            var headers = {'Content-type' : 'application/json'};
            var abici = '{"code": 33, "color": "plateado", "modelo": "rural", "lat": 4.594911, "lng":  -74.123508 }';
            var biciCode = 33;
            
            request.post({
                headers :   headers,
                url :       base_url + '/create',
                body :      abici
            }, function(error, response, body){ 
                
                //result = JSON.parse(body)
                //console.log(result)
                Bicicleta.findByCode(biciCode, (err, bBici) => { 
                    //console.log(bBici)
                    expect(response.statusCode).toBe(200);
                    expect(bBici.color).toBe('plateado');
                    done();
                });
            });
        });
    });

    describe('PUT BICICLETAS /update', ()=>{
        it('status 200', (done)=>{

            
            
            //Se crea el registro a actualizar
            var aBici = new Bicicleta({ code:88, color:'violeta', modelo:'montaña', ubicacion: [-34.6112424, -58.5412424] });
     
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err)
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    //console.log(bicis[0].code)
                    expect(bicis[0].code).toBe(88);
                    
                    var headers = {'content-type':'application/json'}
            
                    var aBici = '{"code":88, "color":"rosado", "modelo":"urbana", "lat":-34.6112424, "lng":-58.5412424}';
                    
                    request.put({
                        headers: headers,
                        url: base_url + '/update/' + 88,
                        body: aBici
                    }, function(error, response, body){   
                        Bicicleta.findByCode(88, (err, bBici) => { 
                            expect(response.statusCode).toBe(200);
                            expect(bBici.color).toBe('rosado');
                            done();
                        });
                    });
                });
            });   
        });
    });


    describe('DELETE BICICLETAS /delete', ()=>{
        it('status 200', (done)=>{



            var aBici = new Bicicleta({ code:15, color:'gris', modelo:'montaña', ubicacion: [-34.6112424, -58.5412424] });


            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err)
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);
          
                    
                });
            });

            var headers = {'content-type':'application/json'}
            var number = '{"code":15}';
                  
            request.delete({
                headers: headers,
                url: base_url + '/delete',
                body: number
                }, function(error, response, body){   
                 
                    expect(response.statusCode).toBe(204);
                    Bicicleta.allBicis(function(err, bicis){
                        
                        //console.log(bicis)
                        expect(bicis.length).toBe(0);
                        done();
                        
                    });
                  
                       
            });
        });
    });

});
 
// describe('Bicicleta API', () => {


//     describe('Get Bicicletas /', () => {
//         it('Debe devolver bici con id 1', () => {    
//             expect(Bicicleta.allBicis.length).toBe(0)


//             var biciB = new  Bicicleta(1, 'negro', 'urbana', [4.594911, -74.123508] )
//             Bicicleta.add(biciB);

//             request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
//                 expect(response.statusCode).toBe(200);

//             });
//         });
//     });

//     describe('Post Bicicletas /create', () => {
//         it('STATUS 200', (done) => {    

//             var headers = {'Content-type' : 'application/json'};
//             var abici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": 4.594911, "lng":  -74.123508 }';
//             //expect(Bicicleta.allBicis.length).toBe(0)

//             request.post({
//                 headers : headers,
//                 url : 'http://localhost:3000/api/bicicletas/create',
//                 body : abici
//             }, function(error, response, body){
//                 expect(response.statusCode).toBe(200);
//                 //console.log(response)
//                 expect(Bicicleta.findById(10).color).toBe("rojo");
//                 done();
//             });
//         });
//     });

//     describe('Post Bicicletas /update', () => {
//         it('STATUS 204', (done) => {    

//             var headers = {'Content-type' : 'application/json'};
//             var abici = '{"id": 100, "color": "rojo", "modelo": "urbana", "lat": 4.594911, "lng":  -74.123508 }';
//             //expect(Bicicleta.allBicis.length).toBe(0)

//             request.post({
//                 headers : headers,
//                 url : 'http://localhost:3000/api/bicicletas/create',
//                 body : abici
//             }, function(error, response, body){
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(100).color).toBe("rojo");
//                 var querybici = '{"id": 100 }'
//                 request.post({
//                     headers : headers,
//                     url : 'http://localhost:3000/api/bicicletas/delete',
//                     body : querybici
//                 }, function(error, response, body){
//                     expect(response.statusCode).toBe(204);
//                     done();
//                 });
                
//             });
//         });
//     });
// });