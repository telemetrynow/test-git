var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta')
var Usuario = require('../../models/usuario')
var Reserva = require('../../models/reserva')



describe('Testing Usuarios', () => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        //mongoose.connect(mongoDB, { useNewUrlParser: true});
        mongoose.connect(mongoDB, { useNewUrlParser: true});


        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log('This is the error: ', err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log('This is the error: ', err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log('This is the error: ', err);
                    done();
                });
            });
        });
        
    });


    describe('Usurio Reservando una bici ', () => {
        it('Debe existir la reserva', (done) => {  
            
            const usuario = new Usuario({nombre: 'Nicolas'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: 'Morado', modelo:'rural'});
            bicicleta.save();


            var hoy = new Date();
            var mañana = new Date();

            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    
                    //console.log(reservas[0]);
                    expect(reservas.length).toEqual(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });    
            });
        });
    });


});