var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta')

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        //mongoose.connect(mongoDB, { useNewUrlParser: true});
        mongoose.connect(mongoDB, { useNewUrlParser: true});


        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log('This is the error: ', err);
            done();
        });
    });

    describe("Bicicleta.createInstance", () => {
            it('Agregar una instancia de una bici', () => {
                
                var bici = Bicicleta.createInstance(1, 'rojo', 'urbana', [4.594911, -74.123508]);
                expect(bici.code).toBe(1);
                expect(bici.color).toBe('rojo');
                expect(bici.modelo).toBe('urbana');
                expect(bici.ubicacion[0]).toEqual(4.594911);
                expect(bici.ubicacion[1]).toEqual(-74.123508);
            });
    });

    describe("Bicicleta.allbicis", () => {
        it('Comienza sin ninguna bici ', (done) => {
            
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0)
                //expect(0).toBe(0);

                done();
            });
        });
    });

    describe("Bicicleta.add", () => {
        it('Agrega unita bici ', (done) => {
            

            var bici = new Bicicleta({code: 24245451, color: 'verde', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});

            Bicicleta.add(bici, function(err, newBici){
                if(err) console.log(err)
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(bici.code);

                    done();
                });
            });
        });
    });


    describe("Bicicleta.findByCode", () => {
        it('Debe devolver la bici con el codigo de 1 ', (done) => {
            
            Bicicleta.allBicis(function(err, bicis){
                
                
                expect(bicis.length).toBe(0)
                
                var bbici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});
                
                Bicicleta.add(bbici, function(err, newBici){
                    if(err)  console.log(err);
                
                    var Bici2 = new Bicicleta({code: 2, color: 'azul', modelo: 'rural', ubicacion: [4.594911, -74.123508]});
                    Bicicleta.add(Bici2, function(err, newBici){
                        if(err)  console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(bbici.code);
                            expect(targetBici.color).toBe(bbici.color);
                            expect(targetBici.modelo).toBe(bbici.modelo);

                            done();
                        });
                    });
                });
            });
            //var bici = new Bicicleta({code: 24245451, color: 'verde', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});
        });
    });

    describe("Bicicleta.removeByCode", () => {
        it('Debe remover la bici con el codigo de 10 ', (done) => {
            
            Bicicleta.allBicis(function(err, bicis){
                
                
                expect(bicis.length).toBe(0)
                
                var bbici = new Bicicleta({code: 10, color: 'morada', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});
                
                Bicicleta.add(bbici, function(err, newBici){
                    if(err)  console.log(err);
                
                    var Bici2 = new Bicicleta({code: 11, color: 'violeta', modelo: 'rural', ubicacion: [4.594911, -74.123508]});
                    Bicicleta.add(Bici2, function(err, newBici){
                        if(err)  console.log(err);
                        Bicicleta.removeByCode(10, function(err, targetBici){
                            
        
                            expect(targetBici.deletedCount).toBe(1);
                            expect(targetBici.ok).toBe(1);
                            
                            expect(targetBici.code).toBe(undefined);
                            expect(targetBici.color).toBe(undefined);
                            expect(targetBici.modelo).toBe(undefined);

                            done();
                        });
                    });
                });
            });
        });
    });


});





// beforeEach(() => {
//     Bicicleta.allBicis = [];
// })


// describe("Bicicleta.allBicis", () => [
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     })
// ]);


// describe("Bicicleta.add", () => [
//     it('Agregar una bici', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
        
//         var aaaa = new Bicicleta(1, 'rojo', 'urbana', [4.594911, -74.123508]);
//         Bicicleta.add(aaaa);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(aaaa);
//     })
// ]);

// describe("Bicicleta.findById    ", () => [
//     it('Debe devolver bici con id 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var biciA = new Bicicleta(1, 'rojo', 'urbana', [4.594911, -74.123508]);
//         var biciB = new Bicicleta(1, 'rojo', 'urbana', [4.594911, -74.123508]);
//         Bicicleta.add(biciA);
//         Bicicleta.add(biciB);

//         var targetbici = Bicicleta.findById(1);
//         expect(targetbici.id).toBe(1);
//         expect(targetbici.color).toBe(biciA.color);
//         expect(targetbici.modelo).toBe(biciA.modelo);

//     })
// ]);